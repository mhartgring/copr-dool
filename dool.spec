Summary: Pluggable real-time performance monitoring tool
Name: dool
Version: 1.3.1
Release: 1%{?dist}
License: GPL
Group: System Environment/Base
URL: https://github.com/scottchiefbaker/dool

Source0: https://github.com/scottchiefbaker/dool/archive/refs/tags/v%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root

BuildArch: noarch
BuildRequires: python >= 3.0
BuildRequires: make
Requires: python >= 3.0

%description
Dool is a versatile replacement for vmstat, iostat, netstat and ifstat.
Dool overcomes some of their limitations and adds some extra features,
more counters and flexibility. Dool is handy for monitoring systems
during performance tuning tests, benchmarks or troubleshooting.

Dool allows you to view all of your system resources in real-time, you
can eg. compare disk utilization in combination with interrupts from your
IDE controller, or compare the network bandwidth numbers directly
with the disk throughput (in the same interval).·

Dool gives you detailed selective information in columns and clearly
indicates in what magnitude and unit the output is displayed. Less
confusion, less mistakes. And most importantly, it makes it very easy
to write plugins to collect your own counters and extend in ways you
never expected.

Dool is a Python3 compatible clone of Dstat.

%prep
%setup

%build

%install
%{__rm} -rf %{buildroot}
%{__make} install DESTDIR="%{buildroot}"

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-, root, root, 0755)
%doc AUTHORS ChangeLog docs/*.html examples/
%doc %{_mandir}/man1/dool.1*
%{_bindir}/dool
%{_datadir}/dool/

%changelog
* Sun Feb 4 2024 Marco Hartgring <marco.hartgring@gmail.com> - 1.3.1-1
- Update to 1.3.1

* Sat Nov 11 2023 Marco Hartgring <marco.hartgring@gmail.com> - 1.3.0-1
- Update to 1.3.0

* Mon Apr 10 2023 Marco Hartgring <marco.hartgring@gmail.com> - 1.1.0-2
- Rebuild for Fedora 38

* Mon Oct 24 2022 Marco Hartgring <marco.hartgring@gmail.com - 1.1.0-1
- Updated to 1.1.0

* Sat Sep 03 2022 Marco Hartgring <marco.hartgring@gmail.com - 1.0.0-2
- Spec file tweaking

* Sun Aug 21 2022 Marco Hartgring <marco.hartgring@gmail.com - 1.0.0-1
- Initial release
